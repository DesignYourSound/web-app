# Rename this file "config.py" and assign the empty values

# Flask secret key
SECRET_KEY = ""

# MySQL variables
MYSQL_DATABASE_HOST = ""
MYSQL_DATABASE_USER = ""
MYSQL_DATABASE_PASSWORD = ""
MYSQL_DATABASE_DB = ""

# Flask-Mail variables
MAIL_SERVER = ""
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USE_SSL = True
MAIL_USERNAME = ""
MAIL_PASSWORD = ""
MAIL_DEFAULT_SENDER = ""

# reCAPTCHA V2
G_CAPTCHA_SITE = ""
G_CAPTCHA_SECRET = ""