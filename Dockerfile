FROM python

COPY requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt

RUN apt update && apt install wkhtmltopdf -y && apt upgrade -y

WORKDIR /designyoursound

RUN mkdir designyoursound instance

COPY designyoursound ./designyoursound

COPY instance/ ./instance

COPY .flaskenv .

RUN groupadd -r designyoursound &&\
    useradd -r -g designyoursound -d /designyoursound -s /sbin/nologin -c "Docker image user" designyoursound &&\
    chown -R designyoursound:designyoursound /designyoursound

USER designyoursound

ENTRYPOINT ["gunicorn"]

CMD ["-w", "4", "--bind", "0.0.0.0:4000", "designyoursound:create_app()"]