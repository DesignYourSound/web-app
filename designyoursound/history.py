from flask import Blueprint
from flask import g
from flask import render_template
from flask import request
from flask import send_from_directory
from flask import redirect
from flask import url_for

from designyoursound.auth import must_be_connected
from designyoursound.extensions import db

bp = Blueprint('history', __name__, url_prefix='/dashboard')

@bp.route('/history', methods=['GET'])
@must_be_connected
def history():
    if request.method == 'GET':
        cursor = db.get_db().cursor()
        cursor.callproc('history_getFromUserId', [g.user['id']])
        files = cursor.fetchall()

        pretty_keys = {
            'module': 'Module',
            'path': 'Fichier',
            'creation_date': 'Date'
        }
        
        return render_template('history.html.jinja2', files=files, pretty_keys=pretty_keys)

@bp.route('/files/<dl>/<id>')
@must_be_connected
def files(dl=None, id=None):
    cursor = db.get_db().cursor()
    cursor.callproc('history_getFromId', [id])
    
    history = cursor.fetchone()
    path = history['path']
    user_id = history['user_id']

    if user_id != g.user['id']:
        return redirect(url_for('dashboard.dashboard'))

    if dl == '1':
        as_attachment = True
    elif dl == '0':
        as_attachment = False
    
    return send_from_directory('files', path + '.pdf', as_attachment=as_attachment)