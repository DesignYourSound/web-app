import hashlib
import time

from flask import g
from flask_mail import Message

import pdfkit

from designyoursound.extensions import db
from designyoursound.extensions import mail

def create_filename(email=None):
    if email:
        user_email = email
    else:
        user_email = g.user['email']
        
    timestamp = time.time()
    
    hash = hashlib.sha1()
    hash.update(str(timestamp).encode('utf-8') + user_email.encode('utf-8'))

    return hash.hexdigest()

def generate_pdf(filename, rendered_template, module):
    path = 'files/'
    css = 'designyoursound/static/css/auth-and-user.min.css'
    
    pdfkit.from_string(rendered_template, 'designyoursound/' + path + filename + '.pdf', css=css)

    cursor = db.get_db().cursor()
    cursor.callproc('history_insert', [filename, g.user['id'], module])
    db.get_db().commit()

    cursor.callproc('history_getIdFromPath', [filename])
    id = cursor.fetchone()['id']

    return id

def send_mail(subject, body, to, reply_to=None):
    if reply_to:
        msg = Message(subject, recipients=[to], body=body, reply_to=reply_to)
    else:
        msg = Message(subject, recipients=[to], body=body)
    
    mail.send(msg)