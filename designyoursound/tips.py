from flask import Blueprint
from flask import render_template
from flask import request
from flask_mail import Message

from designyoursound.auth import must_be_connected
from designyoursound.extensions import db
from designyoursound.extensions import mail
from designyoursound.utils import create_filename
from designyoursound.utils import generate_pdf

bp = Blueprint('tips', __name__, url_prefix='/dashboard/tips')

@bp.route('/room', methods=['GET', 'POST'])
@must_be_connected
def room():
    if request.method == 'POST':
        room_size = request.form['roomSize']
        room_shape = request.form['roomShape']
        room_absorption = request.form['roomAbsorption']

        cursor = db.get_db().cursor()
        cursor.callproc('get_room_tip', [room_shape, room_size, room_absorption])

        tip = cursor.fetchone()

        rendered_template = render_template('tips/room.html.jinja2', tip=tip, is_pdf=True, external=True)
        
        filename = create_filename()
        id_pdf = generate_pdf(filename, rendered_template, 'Conseils : pièce')

        return render_template('tips/room.html.jinja2', tip=tip, is_pdf=False, external=False, id_pdf=id_pdf)
    elif request.method == 'GET':
        cursor = db.get_db().cursor()
        cursor.callproc('room_shape_get')
        shapes = cursor.fetchall()

        cursor.callproc('room_size_get')
        sizes = cursor.fetchall()

        return render_template('tips/room.html.jinja2', shapes=shapes, sizes=sizes)

@bp.route('/equipment', methods=['GET', 'POST'])
@must_be_connected
def equipment():
    if request.method == 'POST':
        has_amplifier = request.form['equipmentAmplifier']

        cursor = db.get_db().cursor()
        cursor.callproc('get_equipment_tip', [has_amplifier])
        tip = cursor.fetchone()

        rendered_template = render_template('tips/equipment.html.jinja2', tip=tip, is_pdf=True, external=True)
        
        filename = create_filename()
        id_pdf = generate_pdf(filename, rendered_template, 'Conseils : équipement')

        return render_template('tips/equipment.html.jinja2', tip=tip, is_pdf=False, external=False, id_pdf=id_pdf)
    elif request.method == 'GET':
       return render_template('tips/equipment.html.jinja2')

@bp.route('/placement', methods=['GET'])
@must_be_connected
def placement():
    return render_template('tips/placement.html.jinja2')