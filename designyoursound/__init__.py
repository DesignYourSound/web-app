from flask import Flask

from designyoursound import admin
from designyoursound import auth
from designyoursound import index
from designyoursound import dashboard
from designyoursound import history
from designyoursound import speaker
from designyoursound import tips
from designyoursound import user
from designyoursound.extensions import bcrypt
from designyoursound.extensions import db
from designyoursound.extensions import mail

def create_app():
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_pyfile("config.py", silent=True)
    
    # Initialize extensions
    bcrypt.init_app(app)
    db.init_app(app)
    mail.init_app(app)

    # Register blueprints
    app.register_blueprint(auth.bp)
    app.register_blueprint(index.bp)
    app.register_blueprint(dashboard.bp)
    app.register_blueprint(speaker.bp)
    app.register_blueprint(tips.bp)
    app.register_blueprint(history.bp)
    app.register_blueprint(user.bp)
    app.register_blueprint(admin.bp)
        
    return app