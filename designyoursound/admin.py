from flask import Blueprint
from flask import flash
from flask import g
from flask import render_template
from flask import redirect
from flask import request
from flask import url_for

from designyoursound.auth import must_be_admin
from designyoursound.extensions import db
from designyoursound.extensions import bcrypt

bp = Blueprint('admin', __name__, url_prefix='/dashboard')

@bp.route('/users', methods=['GET'])
@must_be_admin
def admin_users():
    cursor = db.get_db().cursor()
    cursor.callproc('user_get_admin')
    users = cursor.fetchall()

    pretty_keys = {
        'first_name': 'Prénom',
        'last_name': 'Nom',
        'email': 'Email',
        'status': 'Statut',
        'is_confirmed': 'Confirmé'
    }
    
    return render_template('admin/users/users.html.jinja2', users=users, pretty_keys=pretty_keys)

@bp.route('/user/<id>/update', methods=['GET', 'POST'])
@must_be_admin
def admin_user_update(id=None):
    if request.method == 'GET':
        cursor = db.get_db().cursor()
        cursor.callproc('user_getById', [id])
        user = cursor.fetchone()

        cursor.callproc('status_get')
        status = cursor.fetchall()
        
        return render_template('admin/users/update.html.jinja2', user=user, status=status)

    elif request.method == 'POST':
        first_name = request.form['firstName']
        last_name = request.form['lastName']
        email = request.form['email']
        status_id = request.form['status']

        if 'isConfirmed' in request.form:
            is_confirmed = True
        else:
            is_confirmed = False
        
        cursor = db.get_db().cursor()
        cursor.callproc('user_update_admin', [id, first_name, last_name, email, status_id, is_confirmed])
        db.get_db().commit()

        flash('L\'utilisateur a bien été modifié.', 'success')

        return redirect(url_for('admin.admin_user_update', id=id))

@bp.route('/user/<id>/delete', methods=['GET'])
@must_be_admin
def admin_user_delete(id=None):
    cursor = db.get_db().cursor()
    cursor.callproc('user_delete', [id])
    db.get_db().commit()

    flash('L\'utilisateur a bien été supprimé.', 'success')

    return redirect(url_for('admin.admin_users'))

@bp.route('/user/add', methods=['GET', 'POST'])
@must_be_admin
def admin_user_add():
    if request.method == 'GET':
        cursor = db.get_db().cursor()
        cursor.callproc('status_get')
        status = cursor.fetchall()
        
        return render_template('admin/users/add.html.jinja2', status=status)
    elif request.method == 'POST':
        first_name = request.form['firstName']
        last_name = request.form['lastName']
        email = request.form['email']
        password = request.form['password']
        status_id = request.form['status']

        hashed_password = bcrypt.generate_password_hash(password)

        cursor = db.get_db().cursor()
        cursor.callproc('user_create_admin', [first_name, last_name, email, hashed_password, status_id])
        db.get_db().commit()

        flash('L\'utilisateur a bien été créé', 'success')

        return redirect(url_for('admin.admin_users'))