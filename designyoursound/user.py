from flask import Blueprint
from flask import flash
from flask import g
from flask import render_template
from flask import request

from designyoursound.auth import must_be_connected
from designyoursound.extensions import db
from designyoursound.extensions import bcrypt

bp = Blueprint('user', __name__, url_prefix='/dashboard')

@bp.route('/user/change-password', methods=['GET', 'POST'])
@must_be_connected
def change_password():
    if request.method == 'POST':
        password = request.form['password']
        repeat_password = request.form['passwordRepeat']

        errors = list()
        if not password:
            errors.append('Veuillez renseigner le mot de passe')
        if password != repeat_password:
            errors.append('Les mots de passe ne sont pas identiques.')

        if not errors:
            hashed_password = bcrypt.generate_password_hash(password).decode('utf-8')
            cursor = db.get_db().cursor()
            cursor.callproc('user_update_password', [g.user['id'], hashed_password])
            db.get_db().commit()
            flash('Votre mot de passe a bien été modifié.', 'success')

        for error in errors:
            flash(error, 'error')
        
    return render_template('user/change-password.html.jinja2')

@bp.route('/user/change-email', methods=['GET', 'POST'])
@must_be_connected
def change_email():
    if request.method == 'POST':
        email = request.form['email']

        errors = list()
        if not email:
            errors.append('Veuillez renseigner votre email')

        if not errors:
            cursor = db.get_db().cursor()
            cursor.callproc('user_update_email', [g.user['id'], email])
            db.get_db().commit()
            g.user['email'] = email
            flash('Votre email a bien été modifié.', 'success')

        for error in errors:
            flash(error, 'error')
        
    return render_template('user/change-email.html.jinja2')