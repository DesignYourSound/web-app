from flask import Blueprint
from flask import flash
from flask import g
from flask import render_template
from flask import request

from designyoursound.auth import must_be_connected
from designyoursound.utils import send_mail

bp = Blueprint('dashboard', __name__)

@bp.route('/dashboard', methods=['GET'])
@must_be_connected
def dashboard():
    return render_template('dashboard.html.jinja2')

@bp.route('/dashboard/contact', methods=['GET', 'POST'])
@must_be_connected
def contact():
    if request.method == 'GET':
        return render_template('contact.html.jinja2')
    elif request.method == 'POST':
        subject = request.form['contactSubject']
        text = request.form['contactText']
        
        errors = list()
        if not subject:
            errors.append('Vous devez renseigner le sujet.')
        if not text:
            errors.append('Vous devez renseigner le message.')

        if not errors:
            send_mail('Demande de contact : ' + subject, text, 'contact@designyoursound.fr', reply_to=g.user['email'])
            
            flash('Votre demande de contact à bien été envoyée !', 'success')
            return render_template('contact.html.jinja2')
        
        for error in errors:
            flash(error, 'error')

        return render_template('contact.html.jinja2')
