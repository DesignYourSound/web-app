from flask import Blueprint
from flask import g
from flask import render_template
from flask import request, make_response

from designyoursound.auth import must_be_connected
from designyoursound.extensions import db
from designyoursound.extensions import mail
from designyoursound.extensions import bcrypt
from designyoursound.utils import create_filename
from designyoursound.utils import generate_pdf

bp = Blueprint('speaker', __name__, url_prefix='/dashboard')

@bp.route('/speaker', methods=['GET', 'POST'])
@must_be_connected
def speaker():
    nb_lane = None
    speakers = list()
    id_pdf = None
    
    if request.method == 'POST':
        nb_lane = int(request.form['nbLane'])
        speaker_types = ['bass-reflex']

        if nb_lane == 2:
            speaker_types.append('tweeter')

        cursor = db.get_db().cursor()
        cursor.callproc('get_speaker_ls_vent', [','.join(speaker_types)])

        speaker_ls_vent = cursor.fetchall()

        prefixes = {
            'speaker': 's_',
            'loud_speaker': 'ls_',
            'vent': 'v_'
        }

        for items in speaker_ls_vent:
            speaker_dict = dict()
            ls_dict = dict()
            vent_dict = dict()
            
            for key, value in items.items():
                if key.startswith(prefixes['speaker']):
                    new_key = key.replace(prefixes['speaker'], '')
                    speaker_dict[new_key] = value
                elif key.startswith(prefixes['loud_speaker']):
                    new_key = key.replace(prefixes['loud_speaker'], '')
                    ls_dict[new_key] = value
                elif key.startswith(prefixes['vent']):
                    new_key = key.replace(prefixes['vent'], '')
                    vent_dict[new_key] = value

            speaker_dict['ls'] = ls_dict
            speaker_dict['vent'] = vent_dict
            speaker_dict['filter'] = list()

            cursor.callproc('filter_getByLsIdAndNbLane', [speaker_dict['ls']['id'], nb_lane])
            filters = cursor.fetchall()

            for filter in filters:
                filter['resistors'] = list()
                filter['capacitors'] = list()
                filter['coils'] = list()
                
                cursor.callproc('resistor_getByFilterId', [filter['id']])
                resistors = cursor.fetchall()
                
                cursor.callproc('capacitor_getByFilterId', [filter['id']])
                capacitors = cursor.fetchall()
                
                cursor.callproc('coil_getByFilterId', [filter['id']])
                coils = cursor.fetchall()
                
                for resistor in resistors:
                    filter['resistors'].append(resistor)

                for capacitor in capacitors:
                    filter['capacitors'].append(capacitor)
                
                for coil in coils:
                    filter['coils'].append(coil)

                speaker_dict['filter'].append(filter)

            
            speakers.append(speaker_dict)
        
        rendered_template = render_template('speaker/speaker.html.jinja2', nb_lane=nb_lane, speakers=speakers, is_pdf=True, external=True)
        
        filename = create_filename()
        id_pdf = generate_pdf(filename, rendered_template, 'Enceinte : ' + str(nb_lane) + ' voie(s)')
    
    return render_template('speaker/speaker.html.jinja2', nb_lane=nb_lane, speakers=speakers, is_pdf=False, external=False, id_pdf=id_pdf)