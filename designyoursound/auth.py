from functools import wraps
import requests

from flask import Blueprint
from flask import current_app
from flask import flash
from flask import g
from flask import redirect
from flask import render_template
from flask import request
from flask import session
from flask import url_for

from designyoursound.extensions import bcrypt
from designyoursound.extensions import db
from designyoursound.utils import create_filename
from designyoursound.utils import send_mail

bp = Blueprint('auth', __name__)

@bp.before_app_request
def load_user():
    try:
        user_id = session['user_id']
    except KeyError:
        g.user = None
    else:
        cursor = db.get_db().cursor()
        cursor.callproc('user_GetById', [user_id])
        g.user = cursor.fetchone()

def must_be_connected(view):
    @wraps(view)
    def wrapper(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapper

def must_be_admin(view):
    @wraps(view)
    def wrapper(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))
        
        user_id_status = g.user['id_status']
        
        if user_id_status == 2:
            return redirect(url_for('dashboard.dashboard'))
        elif user_id_status == 1:
            return view(**kwargs)

    return wrapper

@bp.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':        
        first_name = request.form['firstName']
        last_name = request.form['lastName']
        email = request.form['email']
        password = request.form['password']
        repeat_password = request.form['repeatPassword']
        captcha = request.form['g-recaptcha-response']
        
        cursor = db.get_db().cursor()
        cursor.callproc('user_GetByEmail', [email])

        errors = list()
        if not first_name:
            errors.append('Vous devez renseigner votre prénom.')
        if not last_name:
            errors.append('Vous devez renseigner votre nom.')
        if not email:
            errors.append('Vous devez renseigner votre email.')
        if not password:
            errors.append('Vous devez renseigner votre mot de passe.')
        if password != repeat_password:
            errors.append('Vos mots de passe ne sont pas identiques.')
        
        if not captcha:
            errors.append('Vous devez cocher le reCAPTCHA')
        else:
            postData = {
                'secret' : current_app.config['G_CAPTCHA_SECRET'],
                'response' : captcha
            }
            
            req = requests.post('https://www.google.com/recaptcha/api/siteverify', data=postData)
            
            if req.json()['success'] is False:
                errors.append('La vérification reCAPTCHA a echouée')
        
        if cursor.fetchone(): # If there is already an user with the same email
            errors.append('Cet email est déjà utilisé.')
        
        if not errors:
            hashed_password = bcrypt.generate_password_hash(password).decode('utf-8')
            
            hash = create_filename(email)
            
            cursor.callproc('user_Create', [first_name, last_name, email, hashed_password, 2, hash])
            db.get_db().commit()
            
            path = request.url_root + url_for('auth.confirm', hash=hash)
            send_mail('Vérifiez votre compte', 'Bienvenue sur DesignYourSound ! Cliquez sur ce lien pour vérifier votre email : ' + path, email)
            
            flash('Votre compte a bien été créé ! Veuillez vérifier votre email grâce au mail que nous vous avons envoyé.', 'success')
            
            return redirect(url_for('auth.login'))

        # Flashing the error messages
        for error in errors:
            flash(error, 'error')
    
    return render_template('auth/signup.html.jinja2')

@bp.route('/confirm/<hash>', methods=['GET', 'POST'])
def confirm(hash=None):
    cursor = db.get_db().cursor()
    cursor.callproc('user_getByHash', [hash])
    user = cursor.fetchone()

    if user is None:
        return redirect(url_for('index.index'))

    user_id = user['id']

    cursor.callproc('user_confirm', [user_id])
    db.get_db().commit()

    flash('Votre email est confirmée ! Vous pouvez maintenant vous connecter.', 'success')
    
    return redirect(url_for('auth.login'))

@bp.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        cursor = db.get_db().cursor()
        cursor.callproc('user_GetByEmail', [email])
        user = cursor.fetchone()

        errors = list()
        if user is None:
            errors.append('Cet email n\'appartient à aucun compte')
        elif not bcrypt.check_password_hash(user['password'], password):
            errors.append('Mauvais mot de passe')
        elif not user['is_confirmed']:
            errors.append('Votre email n\'est pas encore validée. Vérifier vos mails.')

        if not errors:
            session.clear()
            session['user_id'] = user['id']
            return redirect(url_for('dashboard.dashboard'))
        
        for error in errors:
            flash(error, 'error')
        
    return render_template('auth/login.html.jinja2')

@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index.index'))

@bp.route('/forgot-password', methods=['GET', 'POST'])
def forgot_password():
    if request.method == 'POST':
        email = request.form['email']

        cursor = db.get_db().cursor()
        cursor.callproc('user_getByEmail', [email])
        user = cursor.fetchone()

        errors = list()
        if user is None:
            errors.append('Cet email n\'appartient à aucun compte')

        if not errors:
            lost_password_hash = create_filename(user['email'])
            cursor.callproc('user_setLostHash', [user['id'], lost_password_hash])
            db.get_db().commit()
            
            path = request.url_root + url_for('auth.retrieve_password', hash=lost_password_hash)
            send_mail('Changement de mot de passe', 'Cliquez sur ce lien pour changer votre mot de passe : ' + path, user['email'])
            
            flash('Un mail vous a été envoyé pour changer votre mot de passe', 'success')
            
            return redirect(url_for('auth.login'))

        for error in errors:
            flash(error, 'error')

    return render_template('auth/forgot-password.html.jinja2')

@bp.route('/forgot-password/<hash>', methods=['GET', 'POST'])
def retrieve_password(hash=None):
    if request.method == 'GET':
        return render_template('auth/retrieve-password.html.jinja2', hash=hash)
    elif request.method == 'POST':
        password = request.form['password']
        hash = request.form['hash']

        cursor = db.get_db().cursor()
        cursor.callproc('user_getByLostHash', [hash])
        user = cursor.fetchone()

        if user is None:
            return redirect(url_for('index.index'))

        hashed_password = bcrypt.generate_password_hash(password).decode('utf-8')
        cursor.callproc('user_update_password', [user['id'], hashed_password])
        db.get_db().commit()
        flash('Votre mot de passe bien été modifé !', 'success')

        return render_template('auth/login.html.jinja2')